---
layout: handbook-page-toc
title: "Reachdesk"
description: "Reachdesk is a direct mail provider that we use for sales and marketing campaigns to fuel pipeline progression using personalized gifts"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

# Disclaimer: This page is WIP! We expect Reachdesk to be functional in Q3.

# About ReachDesk
Reachdesk is a direct mail campaign used to land and accelerate deals to fuel pipeline progression using personalized gifts.

## Successful Usage
TBD - FM to fill in

### Reporting and Dashboard
TBD

## How to use 

All campaigns must have a Salesforce.com attached. If this campaign involves Marketo, please skip to [those instructions](handbook/marketing/marketing-operations/reachdesk/#marketo-campaigns), otherwise follow directions [below](handbook/marketing/marketing-operations/reachdesk/#salesforce-campaigns).

Please use this (Mural TO BE CREATED) to determine your setup.

### Salesforce Campaigns
1. Clone [SFDC template](https://gitlab.my.salesforce.com/7014M000001vgGz) and fill in all necessary fields. You must clone because this template contains the  correct member statuses for the integration.
1. Make sure bizible touchpoints are set to `Include only "Responded" Campaign Members`
1. Create your ReachDesk Campaign
1. TBD


### Marketo Campaigns
1. Directions TBD
