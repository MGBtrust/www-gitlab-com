---
layout: handbook-page-toc
title: "Command of the Message"
description: "GitLab has adopted Force Management's Command of the Message customer value-based sales messaging framework and methodology" 
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

GitLab has adopted and operationalized Force Management's Command of the Message value-based sales messaging methodology. Due to IP constraints, we have migrated content from this page to a private [Command of the Message Resource Guide](https://docs.google.com/document/d/1wwNZCkbEv4-WXm8WQmgR6LKjSgxyLZ-wg-Rp9Qav0Zw/edit?usp=sharing) (**GitLab internal only**). 

## Customer Value Drivers

Value drivers describe what organizations are likely proactively looking for or needing and are top-of-mind customer topics that exist even if GitLab doesn’t. Value drivers may cause buyers to re-allocate discretionary funds, and they support a value-based customer conversation. Organizations adopt and implement GitLab for the following value drivers:
1.  **Increase Operational Efficiencies** - _simplify the software development toolchain to reduce total cost of ownership_
1.  **Deliver Better Products Faster** - _accelerate the software delivery process to meet business objectives_
1.  **Reduce Security and Compliance Risk** - _simplify processes to comply with internal processes, controls and industry regulations without compromising speed_

## Resources: Core Content

Please see [Resources: Core Content](https://docs.google.com/document/d/1wwNZCkbEv4-WXm8WQmgR6LKjSgxyLZ-wg-Rp9Qav0Zw/edit#heading=h.ubdqhzd73q97) listed in the Command of the Message Resource Guide.

## GitLab Differentiators

Please see [GitLab Differentiators](https://docs.google.com/document/d/1wwNZCkbEv4-WXm8WQmgR6LKjSgxyLZ-wg-Rp9Qav0Zw/edit#heading=h.6zwr35r6eyfw) described in the Command of the Message Resource Guide.

## Additional Resources

1. [The Mantra](https://docs.google.com/document/d/1wwNZCkbEv4-WXm8WQmgR6LKjSgxyLZ-wg-Rp9Qav0Zw/edit#heading=h.xnjmdw9bzvgf)
1. [Job Aids](https://docs.google.com/document/d/1wwNZCkbEv4-WXm8WQmgR6LKjSgxyLZ-wg-Rp9Qav0Zw/edit#heading=h.toeaw0vfw4e9)
1. [Sharing Feedback](https://docs.google.com/document/d/1wwNZCkbEv4-WXm8WQmgR6LKjSgxyLZ-wg-Rp9Qav0Zw/edit#heading=h.2ztomklbgx8p)
1. [GitLab Value Framework Iteration](https://docs.google.com/document/d/1wwNZCkbEv4-WXm8WQmgR6LKjSgxyLZ-wg-Rp9Qav0Zw/edit#heading=h.coq5jpyfc2aa)
